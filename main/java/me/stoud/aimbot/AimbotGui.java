package me.stoud.aimbot;

import org.lwjgl.input.Keyboard;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import cpw.mods.fml.client.config.GuiSlider;

public class AimbotGui extends GuiScreen{
	private boolean isAimbotOn;
	
	private GuiButton aimbotLabel;
	private GuiButton aimbotToggle;
	
	private GuiSlider aimbotSpeed;
	private GuiButton aimbotSpeedLabel;
	
	private GuiSlider aimbotDistance;
	private GuiButton aimbotDistanceLabel;
	
	private GuiSlider fovSlider;
	private GuiButton fovLabel;
	
	private GuiButton bindLabel;
	private GuiButton bind;
	
	public AimbotGui(){
		//id,xPos,yPos,width, height, String prefix, String suf, double minVal, double maxVal, double currentVal, boolean showDec, boolean drawStr				//
		isAimbotOn = true;
		aimbotLabel = new GuiButton(1, 20, 0, 0, 20, "Aimbot : ");
		aimbotToggle = new GuiButton(1,40,0,20,20,"On");
		
		aimbotSpeedLabel = new GuiButton(8,16,20,0,20,"Speed:");
		aimbotSpeed = new GuiSlider(1, 40, 20, 100, 20, "", "", 10, 50, 20, false, true);
		
		aimbotDistance = new GuiSlider(1,60,40,100,20,"","",30,60,45,false,true);
		aimbotDistanceLabel = new GuiButton(1,25,40,0,20,"Distance: ");
		
		fovLabel = new GuiButton(2,10,60,0,20,"FOV:");
		fovSlider = new GuiSlider(1,30,60,100,20,"","",40,120,70,false,true);
		
		bindLabel = new GuiButton(1,14,80,0,20,"Bind: ");
		bind = new GuiButton(1,26,80,10,20,"F");
	}
	
	@Override
	public void initGui(){
		buttonList.add(aimbotToggle);
		buttonList.add(aimbotLabel);
		buttonList.add(aimbotSpeed);
		buttonList.add(aimbotSpeedLabel);
		buttonList.add(aimbotDistance);
		buttonList.add(aimbotDistanceLabel);
		buttonList.add(fovSlider);
		buttonList.add(fovLabel);
		buttonList.add(bindLabel);
		buttonList.add(bind);
		
	}
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks){
		this.drawDefaultBackground();
		super.drawScreen(mouseX, mouseX, partialTicks);
	}
	
	@Override
	protected void actionPerformed(GuiButton button) {
		if(button == aimbotToggle){
			this.toggleAimbot();
		}
		if(button == bind){
			setBind();
		}
	}

	private void setBind() {
		this.bind.displayString = Keyboard.getKeyName(Keyboard.getEventKey());
	}

	public void toggleAimbot() {
		isAimbotOn = !isAimbotOn;
		this.aimbotToggle.displayString = isAimbotOn ? "On" : "Off";
	}
	
	
	public int getBind(){
		return Keyboard.getKeyIndex(this.bind.displayString);
	}
	
	public boolean isAimbotEnabled(){
		return isAimbotOn;
	}
	
	public double getSpeed(){
		return aimbotSpeed.getValueInt() / 10D;
	}
	
	public double getDistance(){
		return aimbotDistance.getValueInt() / 10D;
	}
	
	public int getFov(){
		return this.fovSlider.getValueInt();
	}
}
