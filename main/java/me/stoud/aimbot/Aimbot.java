package me.stoud.aimbot;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraftforge.common.MinecraftForge;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent.ClientTickEvent;
@Mod(version = "1.0", name = "Aimbot", modid = "Aimbot")
public class Aimbot {

	@EventHandler
	public void init(FMLInitializationEvent event){
		//Called once, sets up Aimbot
		Aimbot aimbot = new Aimbot();
		FMLCommonHandler.instance().bus().register(aimbot);
		MinecraftForge.EVENT_BUS.register(aimbot);
	}

	private Minecraft mc;
	private boolean isGuiOpen;
	private AimbotGui gui;

	private double speed;
	private int fov;
	private double distance;

	public Aimbot(){
		//Basic Initialization of all instance variables
		gui = new AimbotGui();
		mc = Minecraft.getMinecraft();
		isGuiOpen = false;
	}

	@SubscribeEvent
	public void onClientTickEvent(ClientTickEvent event){
		handleGui();
		if(shouldToggle()){
			gui.toggleAimbot();//Terrible design :P
		}

		if(gui.isAimbotEnabled()){ //Terrible design :P
			useAimbot(); //This is the actual aimbot
		}
	}

	private void handleGui() {
		//If Grave is down, open the gui
		if(Keyboard.isKeyDown(Keyboard.KEY_GRAVE)){
			if(!isGuiOpen){
				isGuiOpen = true;
				mc.displayGuiScreen(gui);
			}
		}else if(!(mc.currentScreen instanceof AimbotGui)){
			//If grave isn't down and the AimbotGui isn't open

			//If the gui was open before, set up the values to what they should be
			if(isGuiOpen){
				this.speed = gui.getSpeed();
				this.fov = gui.getFov();
				this.distance = gui.getDistance();
			}
			isGuiOpen = false;
		}
	}

	private EntityPlayer currentEntity; //Current target of the aimbot
	/**
	 * This is the body of the aimbot.
	 * It finds and faces a target player
	 */
	private void useAimbot() {
		//TODO add friends(if requested)
		try{
			
			if(currentEntity == null || mc.thePlayer.getDistanceToEntity(currentEntity) > distance){//TODO second check probably unneeded
				//If we aren't targeting anyone or shouldn't be targeting anyone, find someone to target
				currentEntity = findEntity();
			}else{
				if(shouldFace(currentEntity)){
					//If we should continue to target the current entity, do it
					
					double distanceFromMouse = getDistanceFromMouse(currentEntity);
					double speedToFace;
					if(distanceFromMouse > 8){
						speedToFace = Math.min(2 * speed, (speed * 2D * Math.max(1D/5, distanceFromMouse / mc.gameSettings.fovSetting)));
						faceEntity(currentEntity, speedToFace);
					}
				}else{
					currentEntity = null;//If we shouldn't be facing the current Entity, note that
				}
			}
		}catch(Exception e){}//Empty because this will only occur on NullPointers, at that point the game either hasn't been inited fully or is being closed.
	}

	/**
	 * This method finds an entity to target within the current guidelines
	 */
	private EntityPlayer findEntity() {
		if(mc.theWorld != null){
			//For every player, if we shouldFace it, say we should target that player
			for(Object pllayer : mc.theWorld.playerEntities){
				EntityPlayer player = (EntityPlayer) pllayer;
				if(player.getCommandSenderName().equals(mc.thePlayer.getCommandSenderName())){
					continue;
				}
				if(shouldFace(player)){
					return player;
				}
			}
		}
		return null;
	}

	/**
	 * Checks if the player still is within the correct distance, fov, etc
	 * If so, returns true. Else returns false
	 */
	private boolean shouldFace(EntityPlayer player) {
		if(getDistanceFromMouse(player) < fov){
			if(mc.thePlayer.getDistanceToEntity(player) <= distance){
				if(!player.isInvisible() || hasArmor(player)){
					if(mc.thePlayer.canEntityBeSeen(player)){
						if(!player.isDead){
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	//For keybinds, to make sure we don't register a key as being pressed a ton when only hit once
	//Band-Aid fix, not a good solution but it works(somewhat)
	private long lastTime;
	
	/**
	 * Checks if the aimbot keybind is pressed, if so returns true
	 */
	private boolean shouldToggle() {
		if(Keyboard.isKeyDown(gui.getBind())){
			if(System.currentTimeMillis() - lastTime > 100){
				lastTime = System.currentTimeMillis();
				return true;
			}
		}
		return false;
	}
	//EVERYTHING BELOW HERE ARE UTILITY METHODS.
	//Some are taken from other open source clients.
	
	private boolean hasArmor(EntityPlayer player){
		ItemStack[] armor = player.inventory.armorInventory;
		if(armor != null){
			if(armor[0] != null || armor[1] != null || armor[2] != null || armor[3] != null){
				return true;
			}
		}
		return false;
	}

	public int getDistanceFromMouse(Entity entity)
	{
		float[] neededRotations = getRotationsNeeded(entity);
		if(neededRotations != null)
		{
			float neededYaw =
					mc.thePlayer.rotationYaw
					- neededRotations[0], neededPitch =
					mc.thePlayer.rotationPitch
					- neededRotations[1];
			float distanceFromMouse =
					MathHelper.sqrt_float(neededYaw * neededYaw + neededPitch
							* neededPitch);
			return (int) distanceFromMouse;
		}
		return -1;
	}

	public float[] getRotationsNeeded(Entity entity){
		if(entity == null)
			return null;
		double diffX = entity.posX - mc.thePlayer.posX;
		double diffY;

		if(entity instanceof EntityLivingBase){
			EntityLivingBase entityLivingBase = (EntityLivingBase)entity;
			diffY =
					entityLivingBase.posY
					+ entityLivingBase.getEyeHeight()
					* 0.9
					- (mc.thePlayer.posY + Minecraft
							.getMinecraft().thePlayer.getEyeHeight());
		}else
			diffY =
			(entity.boundingBox.minY + entity.boundingBox.maxY)
			/ 2.0D
			- (mc.thePlayer.posY + Minecraft
					.getMinecraft().thePlayer.getEyeHeight());
		double diffZ = entity.posZ - mc.thePlayer.posZ;
		double dist = MathHelper.sqrt_double(diffX * diffX + diffZ * diffZ);
		float yaw =
				(float)(Math.atan2(diffZ, diffX) * 180.0D / Math.PI) - 90.0F;
		float pitch = (float)-(Math.atan2(diffY, dist) * 180.0D / Math.PI);
		return new float[]{
				mc.thePlayer.rotationYaw
				+ MathHelper.wrapAngleTo180_float(yaw
						- mc.thePlayer.rotationYaw),
						mc.thePlayer.rotationPitch
						+ MathHelper.wrapAngleTo180_float(pitch
								- mc.thePlayer.rotationPitch)};

	}

	public synchronized void faceEntity(Entity cur, double speed2)
	{
		float[] rotations = getRotationsNeeded(cur);
		if(rotations != null){

			mc.thePlayer.rotationYaw = 
					(float) limitAngleChange(mc.thePlayer.prevRotationYaw, rotations[0], speed2);
		}
	}
	public final double limitAngleChange(double current, double intended, double speed2){
		double change = intended - current;
		if(change > speed2)
			change = speed2;
		else if(change < -speed2)
			change = -speed2;
		return current + change;
	}




}
